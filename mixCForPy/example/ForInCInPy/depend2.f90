module depend2
  implicit none

contains

  function twofunc(x) result(y)
    real(8) :: x,y
    y = x*x
  end function twofunc

end module depend2
