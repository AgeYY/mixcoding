module depend1WrapC

  use depend1
  use iso_c_binding

contains

  function onefuncWrapC(x) result(y) bind(c, name='onefuncWrapC')
    real(8), intent(in) :: x
    real(8) :: y
    y = onefunc(x)
  end function onefuncWrapC

end module depend1WrapC
