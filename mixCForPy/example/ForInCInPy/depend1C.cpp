#include <stdio.h>

extern "C"{
  double onefuncWrapC(double* x);
  double hello(double x);
  double onefunc(double x);
  double sumArr(double arr[]);
}

double hello(double x){
  return 2;
}

double onefunc(double x) {
  return onefuncWrapC(&x);
}

double sumArr(double* arr){
  return arr[0] + arr[1];
}

int main(){
  double res;
  res = onefunc(2.0);
  printf("%.2e\n", res);
}
