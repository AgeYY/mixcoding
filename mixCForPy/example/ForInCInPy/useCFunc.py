import ctypes

lib = ctypes.CDLL('./depend1C.so')

prototype = ctypes.CFUNCTYPE(    
    ctypes.c_double,
    ctypes.c_double
)

helloFunc = prototype(('hello', lib))
oneFunc = prototype(('onefunc', lib))

res = oneFunc(ctypes.c_double(2))

print(res)

# Example of how to input and out put array
lib.sumArr.argtypes = [ctypes.POINTER(ctypes.c_double)]
lib.sumArr.restype = ctypes.c_double

arr = (ctypes.c_double * 2)(1.0, 2.0)

sum = lib.sumArr(arr)

print(sum)
