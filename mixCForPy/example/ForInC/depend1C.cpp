#include <stdio.h>

extern "C"{
  double onefuncWrapC(double* x);
}

double onefunc(double x) {
  return onefuncWrapC(&x); // Passing by reference
}

int main() {
  double x = 2;

  printf("onefunc = %.2e", onefunc(x)); // Call fortran function

}
