module depend1WrapC

  use depend1
  use iso_c_binding ! rename your fortran function

contains

  function onefuncWrapC(x) result(y) bind(c, name='onefuncWrapC')
    ! This general idea is to generate a new function which call the original one. All input array must have specific size. Converting high rank array to one dimensional array is highly recommended.
    real(8), intent(in) :: x
    real(8) :: y
    y = onefunc(x)
  end function onefuncWrapC

end module depend1WrapC
