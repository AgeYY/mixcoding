module depend1
  use depend2 ! function in depend2 is required

contains

  function onefunc(x) result(y)
    real(8) :: x,y
    y = twofunc(x)
    y = twofunc(y)
  end function onefunc

end module depend1
