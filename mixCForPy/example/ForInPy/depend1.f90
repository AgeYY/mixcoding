module depend1
  use depend2

contains

  function onefunc(x) result(y)
    real(8) :: x,y

    y = twofunc(x)
    y = twofunc(y)
  end function onefunc

end module depend1
