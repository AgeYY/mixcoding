from numpy.distutils.core import Extension

ext1 = Extension(name = 'fortranCode',
                 sources = ['fortranCode.f90'])

ext2 = Extension(name = 'depend1',
                 sources = ['depend1.f90'])

ext3 = Extension(name = 'depend2',
                 sources = ['depend2.f90'])

if __name__ == "__main__":
    from numpy.distutils.core import setup
    setup(name = 'fortranCode',
          description       = "F2PY Users Guide examples",
          author            = "Pearu Peterson",
          author_email      = "pearu@cens.ioc.ee",
          ext_modules = [ext1, ext3, ext2]
          )
# End of setup_example.py
