module depend2
  implicit none

  type :: lsj_symbol

     integer :: L, S, J

  end type lsj_symbol

contains

  function twofunc(x) result(y)
    real(8) :: x,y
    type(lsj_symbol) :: lsj

    lsj%L = 1
    print *, lsj%L
    y = x*x
  end function twofunc

end module depend2
